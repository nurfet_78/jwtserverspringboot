FROM openjdk:17
ARG JAR_FILE
COPY target/JwtServerSpring-0.0.1-SNAPSHOT.jar application.jar
ENTRYPOINT ["java", "-jar", "application.jar"]

